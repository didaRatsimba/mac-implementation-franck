package com.apple.exercise.applet;

import javacard.framework.Util;
import javacard.security.MessageDigest;

/**
 * This class is the implementation of HMAC hashing algorithm.
 * 
 * @author Franck Rakotomalala
 *
 */
public class HMAC {

    // Inner padding used by HMAC algorithm
    private final static byte[] HMAC_INNER_PADDING = new byte[] { (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36, (byte) 0x36,
        (byte) 0x36, (byte) 0x36, (byte) 0x36 };

    // Outer padding used by HMAC algorithm
    private final static byte[] HMAC_OUTER_PADDING = new byte[] { (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C, (byte) 0x5C,
        (byte) 0x5C, (byte) 0x5C, (byte) 0x5C };

    /**
     * This function computes the HMAC of the given message by using the fixed HMAC_KEY.
     */
    public static byte[] computeHmac (byte[] message, byte[] key) {

        byte[] keyXorIpad = xor (key, HMAC_INNER_PADDING);
        byte[] keyXorIpadConcatMessage = concatenate (keyXorIpad, message);
        byte[] hashKeyXorIpadConcatMessage = hashSHA1 (keyXorIpadConcatMessage);

        byte[] keyXorOpad = xor (key, HMAC_OUTER_PADDING);

        byte[] hmac = hashSHA1 (concatenate (keyXorOpad,
            hashKeyXorIpadConcatMessage));

        return hmac;
    }

    /**
     * This function hashes a message with SHA1 algorithm
     */
    private static byte[] hashSHA1 (byte[] toHash) {
        MessageDigest sha1 = MessageDigest.getInstance (MessageDigest.ALG_SHA,
            false);

        sha1.reset ();

        byte[] hashResult = new byte[sha1.getLength ()];
        sha1.doFinal (toHash, (short) 0, (short) toHash.length, hashResult,
            (short) 0);

        return hashResult;
    }

    /**
     * This function computes XoR operation between two byte arrays
     */
    private static byte[] xor (byte[] key, byte[] padding) {

        // of HMAC Key length is less than ipad or opad
        byte[] hmacKeyPadded = pad (key, (short) padding.length, (byte) 0x00);
        byte[] result = new byte[padding.length];

        for (int i = 0; i < hmacKeyPadded.length; i++) {
            result[i] = (byte) ( hmacKeyPadded[i] ^ padding[i] );
        }
        return result;
    }

    /**
     * This function concatenates two byte arrays
     */
    private static byte[] concatenate (byte[] part1, byte[] part2) {
        byte[] result = new byte[part1.length + part2.length];

        Util.arrayCopy (part1, (short) 0, result, (short) 0,
            (short) part1.length);
        Util.arrayCopy (part2, (short) 0, result, (short) part1.length,
            (short) part2.length);

        return result;
    }

    /**
     * This function adds padding on a given byte array until having the length
     * specified
     */
    private static byte[] pad (byte[] data, short finalLength, byte padd) {
        byte[] padded = new byte[finalLength];

        Util.arrayCopy (data, (short) 0, padded, (short) 0, (short) data.length);

        short counter = (short) data.length;
        while ( counter < finalLength ) {
            byte[] b = new byte[] { (byte) 0x00 };
            Util.arrayCopy (b, (short) 0, padded, counter, (short) 1);
            counter++;
        }
        return padded;
    }
}
